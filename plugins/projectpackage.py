# -*- coding: utf-8 -*-

import collections
import datetime
import itertools
import functools
import os
import random
import re
import string
import sys
import tempfile
import types
import zipfile

import guerilla
import lua


##############################
# Utility classes and function
##############################


class MnemoCache(object):
	"""A descriptor for caching function calls."""

	kwd_mark = object()  # sentinel for separating args from kwargs
	__name__ = 'MnemoCache'  # To avoid crashes on some python interpreter

	def __init__(self, func, cache_duration=None, active=True):

		self.func = func
		self.current_gen = {}  # Specific to generator caching
		self.gen_empty = {}  # Specific to generator caching
		self.duration = cache_duration
		self._active = active
		self.cache = {}

	def _cache_generator_wrapper(self, key):

		for n in itertools.count():
			# First check the cache
			if n < len(self.cache[key]) and self._active:
				yield self.cache[key][n]
			# See if another copy of the generator emptied
			# since our last iteration
			elif self.gen_empty[key]:
				break
			# If none of the above, advance the generator
			# (which may empty it)
			else:
				try:
					term = next(self.current_gen[key][1])
				except StopIteration:
					self.gen_empty[key] = True
					break
				else:
					self.cache[key].append(term)
					yield term

	def flush(self):
		"""Reset the cache content.
		"""

		self.cache = {}
		self.gen_empty = {}
		self.current_gen = {}

	def set_active(self, value):
		"""Sets whether the cache is enabled.

		Args:
			value (bool)
		"""
		assert isinstance(value, bool), (type(value), value)
		self._active = value

	def __call__(self, *args, **kwargs):

		key = args + (self.kwd_mark,) + tuple(sorted(kwargs.items()))

		if not self._active:
			return self.func(*args, **kwargs)

		# Check if there is still a generator waiting to be called
		current_iter_tuple = self.current_gen.get(key, None)

		# The generator is not over, keep generating
		if current_iter_tuple:

			# If the generator is empty and its cache too old, we clean
			if self.duration and (datetime.datetime.now() -
								  current_iter_tuple[0] > self.duration):
				self.gen_empty[key] = False
				self.cache[key] = []
				self.current_gen[key] = (datetime.datetime.now(),
										 self.func(*args, **kwargs))

			return self._cache_generator_wrapper(key)

		# Check if we have the thing in cache
		if key in self.cache:
			r = self.cache[key]

			if self.duration and \
					(datetime.datetime.now() - r[0] > self.duration):
				r = self.func(*args, **kwargs)
				r = (datetime.datetime.now(), r)
				self.cache[key] = r

			return r[1]

		# Not in cache
		r = self.func(*args, **kwargs)

		if isinstance(r, types.GeneratorType):
			# Prepare the cache
			if key not in self.cache:
				self.cache[key] = []

			if key not in self.gen_empty:
				self.gen_empty[key] = False

			if key not in self.current_gen:
				self.current_gen[key] = None

			# Create the generator if it is not consumed yet
			self.current_gen[key] = (datetime.datetime.now(), r)

			return self._cache_generator_wrapper(key)

		r = (datetime.datetime.now(), r)
		self.cache[key] = r

		return r[1]

	def __get__(self, instance, owner):

		# Calling property with no instance, let's return the descriptor
		if not instance:
			return self

		return functools.partial(self.__call__, instance)


def mnemoize_on_args(duration=None, active=True):
	"""Decorator for mnemoizing function return using arguments as key.
	Arguments must always be hashable.

	Args:
		duration (datetime.timedelta, int, None): after being hold this amount
			of time, a cache entry will be invalidated. Integer values will be
			concidered as seconds.
		active (bool): Default active behavior of the cache. Default is true
			(enabled).
	"""

	if isinstance(duration, int):
		duration = datetime.timedelta(seconds=duration)

	def decorator(func):
		cache_obj = MnemoCache(func, duration, active=active)
		return cache_obj

	return decorator


def gen_rand_str(size=10):
	"""Generate a random ascii string of `size` uppercase letters.

	Args:
		size (int): Returned string size.

	Returns:
		(str): Generated random string
	"""
	return ''.join(random.choice(string.ascii_uppercase)
				   for _ in xrange(size))


def sizeof_fmt(num):
	"""Human readable formatting of given `num`of bytes.

	Args:
		num (int): Number of bytes.

	Returns:
		str: Formatted string.
	"""
	for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
		if num < 1024.0:
			return "%3.1f%s" % (num, x)
		num /= 1024.0


def expand_path(path):
	"""Expands the given Guerilla path.

	Args:
		path (str)

	Returns:
		str
	"""
	path = lua.execute('return fs.expand(\"{}\")'.format(path))
	return path.replace("//", "/")


def get_rg_parent(node):
	"""Returns the render graph direct parent of the given node.

	Args:
		node (guerilla.Node)

	Returns:
		guerilla.RenderGraphNode
	"""
	parent = node.getparent()

	while not isinstance(parent, guerilla.RenderGraphNode) and parent:
		parent = parent.getparent()

	return parent


def flow_to_rg_out(node):
	"""Returns True if the given RenderGraphNode flows
	to an output of the render graph.

	Args:
		node (guerilla.RenderGraphNode)

	Returns:
		bool
	"""
	if node.State.get() == "disabled":
		return False

	if guerilla.getclassname(node) == "RenderGraphNodeOutput":
		# It's connected
		return True

	elif guerilla.getclassname(node) == "RenderGraphMacroOutput":
		# Give the RenderGraph macro itself
		outplugs = node.getparent().getoutputs()

	else:
		# We will test all the output plugs of the node
		outplugs = node.getoutputs()

	for outplug in outplugs:
		# We recursively check if the connected nodes flow to the output
		return any((flow_to_rg_out(c.getnode().getparent())
					for c in outplug.Plug.connections(False, True)))

	# We found no connection, let's return
	return False


def is_conn_to_shader_out(shader_node):
	"""Return True if the given ShaderNode is connected
	to the output of the shader graph.

	Args:
		shader_node (guerilla.ShaderNode)

	Returns:
		bool
	"""
	result = False

	if guerilla.isclassof(shader_node, "ShaderOutputNode"):
		return result or True

	for cnode in shader_node.children("ShaderNodeOut", recursive=True):

		connections = cnode.getconnected()
		has_connection = len(connections) > 0

		if has_connection:
			for c in connections:
				parent = c.getparent()
				result = result or is_conn_to_shader_out(parent)
		else:
			result = result or False

	return result


def archive(scenes_references, output_path):
	"""Archive the current Guerilla project to the given output path.
	Only the given scene_references are included in the zip file.

	Args:
		scenes_references (list[SceneFileReference])
		output_path (str)
	"""
	# Get the path of the current scene
	current_scene_path = expand_path(guerilla.Document().getfilename())
	current_scene_name = os.path.basename(current_scene_path)

	# Get the current document string
	current_scene = guerilla.Document().getstringcopy().split("[==[")
	assert len(current_scene) == 2
	current_scene = current_scene[-1]

	zip_ = zipfile.ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED)

	# Create temp directory
	temp_dir = tempfile.mkdtemp(prefix="guerilla_archiver")

	for scene_reference in scenes_references:

		# Copy the files
		for src_path in scene_reference.get_fs_files():
			print "Zipping {}".format(src_path)
			zip_.write(src_path, arcname=os.path.basename(src_path))

		# Modify the path in document copy
		new_path = os.path.join("$(SCENE_PATH)",
								os.path.basename(scene_reference.original_path))
		current_scene = current_scene.replace(scene_reference.original_path,
											  new_path)

	# Save the gproject
	temp_scene = os.path.join(temp_dir, current_scene_name)

	with open(temp_scene, "w") as f:
		f.write(current_scene)

	print "Zipping Document"
	zip_.write(temp_scene, arcname=os.path.basename(temp_scene))
	zip_.close()
	print "Archive complete"


##########################
# Scene File Usage classes
##########################

class SceneFileReference(object):
	"""A file reference in a Guerilla project.
	Utility class to manage file path."""

	SEQUENCE_RE = re.compile(r"(.*)[%$](\d*)[df](.*)")

	def __init__(self, scene_path):
		"""
		Args:
			scene_path (str): path as written in a Guerilla plug.
		"""
		self.__scene_path = scene_path

	@property
	def original_path(self):
		"""Returns the original path as used in the original plug.

		Returns:
			str
		"""
		return self.__scene_path

	@property
	def expanded_path(self):
		"""Returns the expanded path using the local environment.

		Returns:
			str
		"""
		return expand_path(self.__scene_path)

	def is_sequence(self):
		"""Returns True if the given path is to be considered as a sequence.
		Returns False otherwise.

		Returns:
			bool
		"""
		return self.SEQUENCE_RE.match(self.__scene_path)

	def get_fs_files(self):
		"""Returns a list of the files that match the original path.
		Only returns the files that exists on the file system.

		Returns:
			list[str]: list of file systems file
		"""
		# Returns the file paths for the given path
		path = self.expanded_path
		match = self.SEQUENCE_RE.match(path)

		if not match:
			if not os.path.exists(path):
				return []
			else:
				return [path]

		start = match.group(1)
		digit_count = match.group(2)
		end = match.group(3)

		if digit_count:
			digit_regex = "{" + str(int(digit_count)) + "}"
		else:
			digit_regex = "+"

		# Craft the expected regex format
		regex = re.compile(r"{}\d{}{}".format(start, digit_regex, end))

		# Try to find the sequences from the disk
		dir_path = os.path.dirname(path)
		file_paths = next(os.walk(dir_path))[-1]

		all_paths = []

		for file_name in file_paths:

			file_path = os.path.join(dir_path, file_name)

			if not regex.match(file_path):
				continue

			all_paths.append(file_path)

		return all_paths


class SceneReferenceFileUsage(SceneFileReference):
	"""Usage of a path in the Guerilla scene. Associates a file path to the
	plugs that use it."""

	def __init__(self, file_path, plugs, ref_type):
		"""

		Args:
			file_path (str): path as used in the scene
			plugs (list[Plugs]): plugs that uses this file
			ref_type (str): type of file (see SceneReferenceFileType)
		"""
		super(SceneReferenceFileUsage, self).__init__(file_path)

		self.__plugs = plugs
		self.__ref_type = ref_type

	@property
	def type(self):
		"""Returns the type of the reference file. All the types are defined
		in the SceneReferenceFileType class.

		Returns:
			str
		"""
		return self.__ref_type

	def is_used(self):
		"""Returns True if the file is used within the scene.
		A file is used if the node's plug flow to the shader output and if its
		render graph parent node flows to the output
		(neither block or unconnected).

		Returns:
			bool
		"""
		for p in self.__plugs:

			node = p.getnode()

			rg_parent = get_rg_parent(node)

			if not rg_parent:
				return True

			if flow_to_rg_out(rg_parent):
				if isinstance(node, guerilla.ShaderNode):
					if is_conn_to_shader_out(node):
						return True
				else:
					return True

		return False

	def get_plugs(self):
		"""Returns the list of plugs that uses this file.

		Returns:
			list[guerilla.Plugs]
		"""
		return self.__plugs


class SceneReferenceFileType(object):
	"""Structure class to store file type"""
	TEXTURE = "Textures"
	GEOMETRY = "Geometry"
	PROCEDURAL = "Procedurals"

	@classmethod
	def all(cls):
		"""Returns all the defined type.

		Returns:
			list[str]
		"""
		return [cls.TEXTURE, cls.GEOMETRY, cls.PROCEDURAL]


class SceneReferenceFileFilter(object):
	"""For effectively filtering the usage of texture in the scene.
	It uses a cache system which is enabled during the while lifespan of the
	filter object.
	"""

	def __init__(self):
		# Cached versions of the connection functions for speed up.
		# Cache is flushed at every evaluation.
		self.__flow_to_rg = mnemoize_on_args()(flow_to_rg_out)
		self.__connect_to_sh_out = mnemoize_on_args()(is_conn_to_shader_out)

	def filter_unused(self, scene_file_usages):
		"""Generates the scene_file_usages that are used in the scene.

		Args:
			scene_file_usages (list[SceneReferenceFileUsage])

		Yields:
			SceneReferenceFileUsage
		"""

		for sfu in scene_file_usages:

			for p in sfu.get_plugs():

				node = p.getnode()

				rg_parent = get_rg_parent(node)

				if not rg_parent:
					yield sfu
					break

				if self.__flow_to_rg(rg_parent):
					if isinstance(node, guerilla.ShaderNode):
						if self.__connect_to_sh_out(node):
							yield sfu
							break
					else:
						yield sfu
						break


class SceneReferenceFileParser(object):
	"""Parser of all the file usage in the current Guerilla scene."""

	def __init__(self, parent_node=None):
		self.__parent = parent_node or guerilla.Document()

	def get_geo_file_usages(self):
		"""Returns all the geometry file usage in the scene.

		Returns:
			list[SceneReferenceFileUsage]
		"""
		results = collections.defaultdict(list)  # Plugs by path
		# References
		for r in self.__parent.children("ReferenceBase", recursive=True):
			results[r.ReferenceFileName.get()].append(r.ReferenceFileName)

		for r in self.__parent.children("ArchRef", recursive=True):
			results[r.File.get()].append(r.File)

		for r in self.__parent.children("Primitive", recursive=True):
			path = r.GeometryPath.get().split("@")[0]

			if not path:
				continue

			results[path].append(r.GeometryPath)

		for r in self.__parent.children("RenderGraphNodeArchRef",
										recursive=True):
			results[r.File.get()].append(r.File)

		all_usages = []

		for scene_path, plugs in results.items():
			ref_type = SceneReferenceFileType.GEOMETRY
			scene_usage = SceneReferenceFileUsage(scene_path, plugs, ref_type)
			all_usages.append(scene_usage)

		return all_usages

	def get_texture_file_usages(self):
		"""Returns all the texture file usage in the scene.

		Returns:
			list[SceneReferenceFileUsage]
		"""
		results = collections.defaultdict(list)

		for node in self.__parent.children("ShaderNodeSL", recursive=True):

			for child in node.children("ShaderNodeIn", recursive=True):

				if child.PlugName.get() == "File":
					file_path = child.Value.get()

					if file_path == '':
						continue

					results[file_path].append(child.Value)

				if child.PlugName.get() == "Files":
					# First decompose the path
					file_paths = child.Value.get().splitlines()

					for file_path in file_paths:
						results[file_path].append(child.Value)

		# Texture attributes
		for child in self.__parent.children("AttributeShader", recursive=True):

			for p in child.plugs(typename="texture"):
				results[p.get()].append(p)

		# Texture plugs
		for child in self.__parent.children("RenderGraphNodeShader",
											recursive=True):

			for p in child.plugs(typename="texture"):
				results[p.get()].append(p)

		all_usages = []

		for scene_path, plugs in results.items():
			ref_type = SceneReferenceFileType.TEXTURE
			scene_usage = SceneReferenceFileUsage(scene_path, plugs, ref_type)
			all_usages.append(scene_usage)

		return all_usages

	def get_procedurals(self):
		"""Returns all the procedural files used in the scene.

		Returns:
			list[SceneReferenceFileUsage]
		"""
		results = collections.defaultdict(list)

		for node in self.__parent.children("Yeti", recursive=True):
			path = node.File.get()
			results[path].append(node.File)

		all_usages = []

		for scene_path, plugs in results.items():
			ref_type = SceneReferenceFileType.PROCEDURAL
			scene_usage = SceneReferenceFileUsage(scene_path, plugs, ref_type)
			all_usages.append(scene_usage)

		return all_usages

	def get_file_usages(self):
		"""Returns all the file usage in the scene.

		Returns:
			list[SceneReferenceFileUsage]
		"""
		return self.get_geo_file_usages() + self.get_texture_file_usages() + \
			   self.get_procedurals()


#######################
# GUI definition
#######################


def _lua_table_tree(python_list):
	"""Utility function that convert a python dict to a lua string to load a
	Guerilla tree widget.

	The input python list is expected to have the following structure:

	[
		"Item1",  # Not expandable node with no children
		"Item2",
		{
			"Name": "Item3",  # dict defines a expandable node with children
			"children":
			[
				"Item4",
				{
					"Name": "Item5",
					"children":
					[
						"Item6"
					]
					]
				}
			]
		}
	]

	When loaded in a Guerilla tree Widget, it will look like this:

	Item1
	Item2
	v Item3
		Item4
		v Item5
			Item6
	"""

	def __recurse(obj):

		res_str = ""

		if isinstance(obj, dict):
			res_str += "{" + "Name=\"{}\", ".format(obj["Name"])
			children = obj.get("children", None)

			if children is not None:
				for c in children:
					res_str += __recurse(c)
			res_str += "}, "
		else:
			res_str += "\"{}\", ".format(obj)

		return res_str

	res = "{"

	for k in python_list:
		res += __recurse(k)

	res += "}"
	return res


class CheckBox(object):
	"""Wrapper over the lua internal check box widget"""

	def __init__(self, text, parent):
		self.__spacing = 8

		# Create lua parent, used in next statement
		lua_parent = guerilla.toLua(parent)
		# Create the check box in lua and get it back
		self.__cb = guerilla.fromLua(lua.execute(
			"cb = LUICheckBoxCreate(\"{}\", python.locals("
			").lua_parent); cb:eval(); return cb".format(
				"CheckBox" + gen_rand_str())))
		# HACK: Check box shows with wrong texture. Focus it to fix the display
		self.__cb.setfocus()

		# Adjust the position of check box in parent space
		self.__cb.setx(2, 0, 0)
		self.__cb.sety(2, 0, 0)

		# Create text to display
		self.__text = guerilla.ui.text("Label" + gen_rand_str(), parent)
		self.__text.settext(text)
		self.__text.setx(self.__cb.getw() + self.__spacing, 0, 0)

	def is_checked(self):
		"""Returns True if the check box is checked.

		Returns:
			bool
		"""
		return self.__cb.Output.get() is True

	@property
	def height(self):
		"""Returns the height of the widget in pixels.

		Returns:
			int
		"""
		return self.__cb.geth()

	def set_position(self, x, y):
		"""Sets the position of the widget relative to its parent.

		Args:
			x (int)
			y (int)
		"""
		self.__cb.setx(x, 0, 0)
		self.__cb.sety(y, 0, 0)
		self.__text.setx(self.__cb.getx() + self.__cb.getw() + self.__spacing,
						 0, 0)
		self.__text.sety(y, 0, 0)

	def move(self, x, y):
		"""Moves the widget for its current position.

		Args:
			x (int)
			y (int)
		"""
		self.__cb.setx(self.__cb.getx() + x, 0, 0)
		self.__cb.sety(self.__cb.gety() + y, 0, 0)
		self.__text.setx(self.__cb.getx() + self.__cb.getw() + self.__spacing,
						 0, 0)
		self.__text.sety(self.__cb.gety() + self.__cb.geth() + self.__spacing,
						 0, 0)

	def set_value_changed_callback(self, callback):
		"""Sets the callback when the state of the check bbox changes.

		Args:
			callback (func)
		"""

		lua_cb = guerilla.toLua(self.__cb)  # Necessary for lua, do not delete
		lua.execute(
			"widget = python.locals().lua_cb; widget._onLClick = "
			"widget.onLClick; local callback = python.locals().callback; "
			"function widget:onLClick() self:_onLClick();callback() end")


class PackageProjectWindow(object):
	"""Main window of the package project tool."""

	def __init__(self):

		# Init the data
		self.__text_to_plugs = {}
		self.__scene_usages = SceneReferenceFileParser().get_file_usages()
		self.__doc_str = guerilla.Document().getstringcopy()

		# UI setup
		self.__margin = 15
		self.__spacing = 15

		self.__mw = guerilla.ui.titlewindow("ArchiverMainWindow", None)

		self.__cb_unused = CheckBox("Unused Files", self.__mw)
		self.__btn = guerilla.ui.textbutton("Button" + gen_rand_str(),
											self.__mw, "Package Project")
		self.__tree = guerilla.ui.tree("Tree" + gen_rand_str(), self.__mw)
		self.setup_ui()
		self._reload_ui()

	def setup_ui(self):
		"""Prepare the ui"""
		self.__mw.settitle("Package Project")
		self.__mw.setw(400, 0, 0, 0)
		self.__mw.seth(250, 0, 0, 0)

		self.__cb_unused.set_position(10, 10)

		self.__btn.setw(120, 0, 0, 0)
		self.__btn.seth(40, 0, 0, 0)

		self.__btn.setx(0, 0.5, 0.5)
		self.__btn.sety(-self.__margin, 1, 1)

		self.__build_tree()

		lua_btn = guerilla.toLua(self.__btn)
		callback = self._btn_clicked
		lua.execute(
			"local callback = python.locals().callback; widget=python.locals("
			").lua_btn; function widget:buttonclicked() callback() end")

		self.__btn.setfocus()
		self.__cb_unused.set_value_changed_callback(self._reload_ui)

	def __build_tree(self):
		"""Builds tree after deleting the old one.
		Guerilla does not refresh the display when you set the data of the
		tree so we rebuild it when the inputs changed."""

		self.__tree.destroy()

		self.__tree = guerilla.ui.tree("Tree" + gen_rand_str(), self.__mw)
		self.__tree.setx(10, 0, 0)
		self.__tree.sety(-self.__margin - self.__spacing - self.__btn.geth(),
						 1, 1)
		height = 2 * self.__margin + 2 * self.__spacing + \
				 self.__btn.geth() + self.__cb_unused.height
		self.__tree.seth(-height, 1, 0, 0)
		self.__tree.setw(-self.__margin, 1, 0, 0)

		lua_tree = guerilla.toLua(self.__tree)

		# Remove the existing commands on the tree and add the new ones
		cmd_callback = self._cmd_select_plug_tree
		selection_enabled = self._plug_selection_valid
		lua_plug_cmd = lua.execute("""
			local tree = python.locals().lua_tree
			local cmd = command.create("Select Plugs")
			local callback = python.locals().cmd_callback
			local valid_selection = python.locals().selection_enabled
			
			function cmd:action()
				callback()
			end
		
			function cmd:isenabled()
				return valid_selection()
			end

			function tree:onGetMenuCommands(commands)
			
				for k in pairs(commands) do
					commands[k] = nil
				end

				table.insert(commands, cmd)
			end
		""")
	
	def _plug_selection_valid(self):
		if isinstance(self.__tree.getselection(), list):
			return False

		return self.__tree.getselection() in self.__text_to_plugs

	def _cmd_select_plug_tree(self, *args, **kwargs):
		plugs = self.__text_to_plugs[self.__tree.getselection()]
		nodes = set([])

		for p in plugs:
			nodes.add(p.getnode())

		with guerilla.Modifier() as mod:
			mod.select(nodes, mode='replace')

	def _reload_ui(self):
		"""Reloads the whole UI."""
		# TODO: The construction of the displayed data is a bit ugly.
		#  Needs refactoring. For now, I first build all the workable data,
		#  sort them and we create the displayed text.

		# Create a filter object
		scene_filter = SceneReferenceFileFilter()
		all_usages = self.__scene_usages

		if not self.__cb_unused.is_checked():
			all_usages = list(scene_filter.filter_unused(all_usages))

		# Sort data per category
		cat_usage = collections.defaultdict(list)

		for usage in all_usages:
			cat_usage[usage.type].append(usage)

		# Compute workable data
		all_cat_data = []

		for category_name, scene_usages in cat_usage.items():

			cat_data = []

			if not scene_usages:
				all_cat_data.append([category_name, 0, 0, False, cat_data])
				continue

			cat_size = 0
			cat_file_count = 0
			cat_not_found = 0

			for scene_usage in scene_usages:

				if not scene_usage.is_sequence():
					fs_files = scene_usage.get_fs_files()

					if not fs_files:
						cat_not_found = True
						d = [scene_usage.original_path, 0, 0, True, [], []]
					else:
						assert len(fs_files) == 1
						f = fs_files[0]
						size = os.path.getsize(f)
						cat_size += size
						cat_file_count += 1
						file_data = [f, size]
						d = [scene_usage.original_path, 1, size, False,
							 [file_data], scene_usage.get_plugs()]

					cat_data.append(d)
					continue

				fs_files = scene_usage.get_fs_files()

				if not fs_files:
					cat_not_found = True
					d = [scene_usage.original_path, 0, 0, True, [], []]
					cat_data.append(d)
					continue

				all_file_data = []
				file_full_size = 0

				for f in fs_files:
					size = os.path.getsize(f)
					file_full_size += size
					file_data = [f, size]
					all_file_data.append(file_data)

				all_file_data.sort(key=lambda d_: d_[1], reverse=True)
				cat_file_count += len(fs_files)
				cat_size += file_full_size
				d = [scene_usage.original_path, len(fs_files), file_full_size,
					 False, all_file_data, scene_usage.get_plugs()]
				cat_data.append(d)

			cat_data.sort(key=lambda d_: d_[2], reverse=True)
			all_cat_data.append([category_name, cat_file_count, cat_size,
								 cat_not_found, cat_data])

		all_cat_data.sort(key=lambda d_: d_[2], reverse=True)

		# Create display text
		document_size = sys.getsizeof(self.__doc_str)
		cat_tree_data = ["Document {0}".format(sizeof_fmt(document_size))]

		for cat_name, file_count, size, not_found, cat_data in all_cat_data:

			if not cat_data:
				cat_tree_data.append("{}: no files".format(cat_name))
				continue

			not_found_msg = ""
			if not_found:
				not_found_msg = " (some not found)"

			usage_tree_data = []
			

			for path, ufile_count, usize, unot_found, usage_data, plugs in cat_data:

				if not usage_data:
					text = "NOT FOUND - {0}".format(path)
					usage_tree_data.append(text)
					continue

				# usage_children = []
				#
				# for usage_child in usage_data:
				#     text = "{0}: {1}".format(usage_child[0],
				#                              sizeof_fmt(usage_child[1]))
				#     usage_children.append(text)

				usage_text = "{0}: {1} files - {2}" \
							 "".format(path, ufile_count, sizeof_fmt(usize))
				self.__text_to_plugs[usage_text] = plugs
				usage_tree_data.append(usage_text)
				
				# usage_tree_data.append({"Name": usage_text,
				#                         "children": usage_children})

			text = "{0}: {1} files{2} - {3}" \
				   "".format(cat_name, file_count, not_found_msg,
							 sizeof_fmt(size))
			cat_tree_data.append({"Name": text, "children": usage_tree_data})

		# Create total
		not_found_msg = " (some not found)" \
			if any((cat[3] for cat in all_cat_data)) else ""
		doc_file_count = sum(cat[1] for cat in all_cat_data)
		doc_file_size = sum(cat[2] for cat in all_cat_data)
		total_msg = "Total: {0} files {1} - {2}" \
					"".format(doc_file_count, not_found_msg,
							  sizeof_fmt(doc_file_size))

		# Create a fresh new tree
		self.__build_tree()

		# Feed it
		all_display_data = [{"Name": total_msg, "children": cat_tree_data}]
		tree = guerilla.toLua(self.__tree)
		tree_data = _lua_table_tree(all_display_data)
		lua.execute("python.locals().tree:settree({})".format(tree_data))

		# Expand the root items
		lua.execute(
			"local tree = python.locals().tree; "
			"for var=1,tree.Root.Count do tree:expandItem(tree.Root[var]) end")

	def _btn_clicked(self):
		"""Opens a dialog to export the archive."""
		# Open a file name window
		current_file_path = guerilla.Document().getfilename()
		current_dir = None
		current_name = None

		if current_file_path:
			current_dir = os.path.dirname(current_file_path)
			current_name = os.path.basename(current_file_path)

		filename = guerilla.getsavefilename("Package Project ...", "*.zip",
											current_dir, None, current_name)[0]

		if not filename:
			return

		# Run the archive process
		archive_project(filename, self.__cb_unused.is_checked())

		# Close the window
		self.__mw.hide()
		self.__mw.destroy()

	def show(self):
		"""Shows the main window."""
		self.__mw.show()


###########################
# Commands
###########################

class ShowPackageProjectDialogCommand(guerilla.command):
	"""Guerilla command to open the window for packaging project"""

	@staticmethod
	def action(luaObj, window, x, y, suffix):
		window = PackageProjectWindow()
		window.show()


def archive_project(output_path, unused=False):
	"""Macro function to package the project to a zip file containing the
	sources files filtered depending on arguments.

	Args:
		output_path (str): path to the output zip file
		unused (bool, optional): If True, will also contains the unused
			resource files.
	"""
	# Parse the document to get the resource files
	file_refs = SceneReferenceFileParser().get_file_usages()

	# Filters
	scene_filter = SceneReferenceFileFilter()
	file_refs = (f for f in scene_filter.filter_unused(file_refs) if not unused)

	archive(list(file_refs), output_path)


def show_gui():
	"""Shows the Project packager GUI."""
	window = PackageProjectWindow()
	window.show()


def register_menu():
	"""Register the Project packager tool in the File menu."""

	# Skip if not in GUI
	if not lua.execute("return GAGUI"):
		return

	# Associate the command to the File Menu
	cmd = ShowPackageProjectDialogCommand('Package Project')
	cmd.install('File')



if __name__ == '__main__':
    show_gui()
