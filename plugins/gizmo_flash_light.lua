
local GizmoFlashLightCreate, GizmoFlashLightBuildMesh

-- %class ShaderNodeFlashLight
-- %desc A barn door gizmo
--[[
%long This class implements a simple pyramidal frustum shader node, that can be
used to control a light shader, for instance. This class handles vertical and
horizontal borders separately, using H Angle and V Angle outputs. Optionnaly,
falloff values are also handled.
]]
class ("ShaderNodeFlashLight", "ShaderNodeGizmo")
-- Declare the list of variable this gizmo handles. For a simple gizmo, only handle
-- H Angle and V Angle.
ShaderNodeFlashLight.Variables =
{
	"Angle", "Delta Angle", "Base Radius", "Delta Base Radius"
}
-- Callback when the display wants to show the actual 3d gizmos of the shader node
function ShaderNodeFlashLight:onGetGizmos (tool, gizmos)
	-- V Angle and H Angle must be present
	local	a, da, b, db, col = self:getinput ("Angle"), self:getinput ("Delta Angle"), self:getinput ("Base Radius"), self:getinput ("Delta Base Radius"), self.getinput("Color")

	if a and vd and hl and hr and col then
		-- create the 3d objects (gizmos) for horizontal and vertical manipulation
		table.insert (gizmos, GizmoFlashLightCreate (tool, self, false, a.Value, nil, b.Value, nil, col.Value))
		table.insert (gizmos, GizmoFlashLightCreate (tool, self, true, a.Value, nil, b.Value, nil, col.Value))

		if da and db then
		    table.insert (gizmos, GizmoFlashLightCreate (tool, self, false, a.Value, da.Value, b.Value, db.Value, col.Value))
		    table.insert (gizmos, GizmoFlashLightCreate (tool, self, true, a.Value, da.Value, b.Value, db.Value, col.Value))
		end
	end
end

function ShaderNodeFlashLight:getargs ()
	local	a, da, b, db = self:getinput ("Angle"), self:getinput ("Delta Angle"), self:getinput ("Base Radius"), self:getinput ("Delta Base Radius")
	if a and da and b and db then
		return "flashlight", { a.Value:get (), da.Value:get (), b.Value:get (), db.Value:get () }
	end
end

-- %class ShaderNodeFlashLightFallof
-- %desc A frustum gizmo with fallof
class ("ShaderNodeFlashLightFallof", "ShaderNodeFlashLight")
-- Handle all outputs
ShaderNodeFlashLightFallof.Variables =
{
	"Angle", "Delta Angle", "Base Radius", "Delta Base Radius"
}

-- Note: Gizmos are only available with interface, it is not legal to subclass them when GAGUI is not true
if GAGUI then

-- Create the GizmoFlashLight class, from the base class of 3d gizmos LUIGizmo3D
-- LUIGizmo3D objects implement most of the work needed to be done for 3D manipulation
-- in the viewport. Yet, there are a few methods that need to be implemented in order
-- for the gizmo to actually interact with the scene graph. The following methods
-- must be implemented.
class ("GizmoFlashLight", "Gizmo3D")
-- Callback when the user clicks on the gizmo. This callback lets you initiate the beginning
-- of a click and drag modification. x and y are the mouse coordinates in the window, view
-- is the viewport in which the user clicked.
function GizmoFlashLight:onLDown (x, y, view)
	-- call the parent class left down handler (required)
	GizmoFlashLight.Parent.onLDown (self, x, y, view)
	local targetObject = self._TargetObject
	-- the transform of the target primitive
	local mtx = self:getpathworldmatrix (self._Path, targetObject)
	local p0 = mtx:gettranslation ()
	-- the position on the target primitive
	-- distance to target
	local length = targetObject.TargetDistance:get ()
	assert (length)

	self._StartPBase = mtx:transform ({ 0, 0, 0 })
	self._StartP = mtx:transform({0, 0, length})

	-- _StartN is the front vector (Z axis for a target primitive)
	self._StartN = mtx:getk ():getnormalized ()

	self._StartD = (p0 - self._StartP):getlength ()
	-- LUI3d2dToPlane projects the x,y coordinates into a 3D position that lies on the 3D plane made by
	-- _StartP and perpendicular to _StartN
end
-- Callback when the user drags the gizmo. view is the current viewport, x,y are
-- the mouse offset since last call to onDrag, and lx,ly are the mouse coordinates
-- in the viewport
function GizmoFlashLight:onDrag (view, x, y, lx, ly)

	-- perform the modification
	if self._IsBase then
		-- compute the position of the mouse on the plane
		local p = LUI3d2dToPlane (view, lx, ly, self._StartPBase, self._StartN)

		if self._DeltaBasePlug and self._BasePlug:get() ~= 0 then
		    local vec = p-self._StartPBase

			local newSize = self._StartA:dotproduct ((p-self._StartPBase)) * 2 * self._StartSignBase / self._StartScale
			self:domodification(newSize / self._BasePlug:get())
		else
			local newSize = self._StartA:dotproduct ((p-self._StartPBase)) * 2 * self._StartSignBase / self._StartScale
			self:domodification(newSize)
		end
	else
		-- compute the position of the mouse on the plane
		local p = LUI3d2dToPlane (view, lx, ly, self._StartP, self._StartN)
		-- compute the angle of this point to the center axis

		if self._DeltaAnglePlug then
			-- if the gizmo is a delta gizmo, then compute the delta from the new angle
			-- and the outer angle
			local opp = (self._StartA:dotproduct (p - self._StartP) - self._StartSignAngle * self._DeltaBasePlug:get() * self._BasePlug:get() * 0.5 * self._StartScale)
			local newAngle = math.atan (opp / self._StartD) * self._StartSignAngle
			self:domodification (self._AnglePlug:get () - newAngle)
		else
			local tan = (self._StartA:dotproduct (p - self._StartP) - self._StartSignBase * self._BasePlug:get() * 0.5 * self._StartScale) / self._StartD
			local newAngle = math.atan (tan) * self._StartSignBase
			-- if the gizmo is the outer gizmo, then just modify with the computed angle
			self:domodification (newAngle)
		end
	end

	return 0,0
end
-- Callback when the viewport wants to read the current gizmo value
function GizmoFlashLight:onGetGizmoValue ()

	if self._IsBase then
		if self._DeltaBaseRadiusPlug then
			return self._DeltaBaseRadiusPlug:get()
		else
			return self._BaseRadiusPlug:get()
		end
	end

	if self._DeltaAnglePlug then
		-- for delta gizmo, return the delta plug value
		return self._DeltaAnglePlug:get ()
	else
		-- for outer gizmo, return the angle
		return self._AnglePlug:get ()
	end
end
-- Callback when the viewport wants to change the current gizmo value
-- This function is always called in a modification context, that is
-- you have to get a modifier on the document and use this to change the
-- associated plug value
function GizmoFlashLight:onSetGizmoValue (value)
	local	mod = Document:getmodifier ()
	if self._IsBase then
		if self._DeltaBaseRadiusPlug then
			mod.set(self._DeltaBaseRadiusPlug, math.min(1.0, math.max(value, 0.0)))
		else
			mod.set(self._BaseRadiusPlug, math.max(value, 0))
		end
	elseif self._DeltaAnglePlug then
		mod.set (self._DeltaAnglePlug, math.max (value, 0))
	else
		mod.set (self._AnglePlug, math.min (math.max (value, 0), 90))
	end
end
-- Callback when the system wants the object to evaluate a plug value
function GizmoFlashLight:eval (plug)
	if plug == self.Transform then
		-- The gizmo transform is the associated object transform
		local targetObject = self._TargetObject
		--return transform.create (targetObject.Transform:get ())
		return transform.create (true)
	elseif plug == self.MeshFlags then
		-- no specific mesh flags
		return 0
	elseif plug == self.MeshScaleFactor then
		-- don't change the gizmo mesh scale
		return 1
	elseif plug == self.Mesh then
		-- build the gizmo mesh
		local targetObject = self._TargetObject
		local angle = self._AnglePlug:get ()
		local baseradius = self._BaseRadiusPlug:get()
		if self._DeltaAnglePlug then
			deltaangle = angle - self._DeltaAnglePlug:get ()
		end
		if self._DeltaBaseRadiusPlug then
			baseradius = baseradius * self._DeltaBaseRadiusPlug:get()
		end
		local length = targetObject.TargetDistance:get ()
		local dir = targetObject.DirectionMode:get () == "-z" and -1 or 1
		-- this function builds the 3D object for this gizmo
		print('Build Mesh')
		return GizmoFlashLightBuildMesh (self, length*dir, self._IsBase, angle, baseradius)
	end
	-- the plug could not be evaluated, let the parent class evaluate the plug
	return self:parenteval (plug, "LUIGizmo3D")
end
-- Retrieve the GL properties of the gizmo
function GizmoFlashLight:getGLprops ()
	return GLdisplay.OverrideDisplayMode+GLdisplay.NodeLocal+self._TargetObject:getGLnodetype (), 1
end

-- We create a subclass for each kind of gizmos. This is important because
-- the manipulation system automatically performs the modification on all
-- active gizmos of the same class. So, if all gizmos were of the same class
-- dragging a gizmo would cause all gizmos of this class to react, and it wouldn't
-- be possible to separately change the frustum height and width.
class ("GizmoAngleFlashLight", "GizmoFlashLight")
class ("GizmoDeltaAngleFlashLight", "GizmoFlashLight")
class ("GizmoBaseRadiusFlashLight", "GizmoFlashLight")
class ("GizmoDeltaBaseRadiusFlashLight", "GizmoFlashLight")

-- This function create a simple gizmo. A whole frustum is composed of 2 gizmos, one for
-- horizontal manipulation and one for vertical. Fallof gizmo is an additionnal 2 gizmos.
function GizmoFlashLightCreate (tool, targetObject, isBase, aPlug, daPlug, bPlug, dbPlug, colorPlug)
	-- Create a 3D gizmo that holds 1 mesh

	local	self = LUIGizmo3DCreate (tool,
		{
			LUIGizmoMeshCreate (nil, { 1, 0, 1, 1 }),
		}, targetObject)
	-- Set the accurate class depending on the kind of gizmo that is required

	if isBase then
        setclass (self, GizmoDeltaBaseRadiusFlashLight and dbPlug or GizmoBaseRadiusFlashLight)
	else
        setclass (self, GizmoDeltaAngleFlashLight and daPlug or GizmoAngleFlashLight)
	end

	assert (targetObject)
	assert (colorPlug)

	self._IsBase = isBase
	self._TargetObject = targetObject
	self._AnglePlug = aPlug
	self._DeltaAnglePlug = daPlug
	self._BaseRadiusPlug = bPlug
	self._DeltaBaseRadiusPlug = dbPlug

	-- The mesh plug
	Plug (self, "Mesh", Plug.NoSerial, types.mesh)

	-- the gizmo mesh is dependent on the input plugs
	self.Mesh:adddependencies (aPlug, bPlug, targetObject.TargetDistance, targetObject.DirectionMode)

	if daPlug and dbPlug then
	    self.Mesh:adddependencies (daPlug, dbPlug)
    end

	-- the gizmo transform is dependent on the associated object transform
	self.Transform:set (transform.create (true))
	-- connect the gizmo to the given input plugs
	self._Meshes[1].Geometry.Mesh:connect (self.Mesh)
	self._Meshes[1].GeometryTransparent.Mesh:connect (self.Mesh)
	self._Meshes[1].Color:connect (colorPlug)
	return self
end

setglobal ("GizmoFlashLightCreate", GizmoFlashLightCreate)

-- Build the actual 3D mesh for the gizmo
function GizmoFlashLightBuildMesh (self, length, isBase, angle, baseradius)

	local line = LUIMeshCreate ()

	if isBase then
	    local ctlpointscount = 32
	    -- Place multiple points
	    local stepangle = math.pi / ctlpointscount

        local ctlpoints = {}

	    for i = 1, ctlpointscount do
	        table.insert(ctlpoints, {math.cos((i-1)*stepangle), math.sin((i-1)*stepangle), length})
	    end

	    LUIMeshAddPolyLine(line, ctlpoints, false)
    end
	local mesh, laabb = glmesh.createlines (line, 0)
	return { mesh, laabb }
end

end -- GAGUI
