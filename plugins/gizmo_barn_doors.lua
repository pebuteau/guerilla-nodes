
local GizmoBarnDoorsCreate, GizmoBarnDoorsBuildMesh

-- %class ShaderNodeBarnDoors
-- %desc A barn door gizmo
--[[
%long This class implements a simple pyramidal frustum shader node, that can be
used to control a light shader, for instance. This class handles vertical and
horizontal borders separately, using H Angle and V Angle outputs. Optionnaly,
falloff values are also handled.
]]
class ("ShaderNodeBarnDoors", "ShaderNodeGizmo")
-- Declare the list of variable this gizmo handles. For a simple gizmo, only handle
-- H Angle and V Angle.
ShaderNodeBarnDoors.Variables =
{
	"H Angle Left", "H Angle Right", "V Angle Up", "V Angle Down", "H Base", "V Base"
}
-- Callback when the display wants to show the actual 3d gizmos of the shader node
function ShaderNodeBarnDoors:onGetGizmos (tool, gizmos)
	-- V Angle and H Angle must be present
	local	vu, vd, hl, hr, vb, hb, col = self:getinput ("V Angle Up"), self:getinput ("V Angle Down"), self:getinput ("H Angle Left"), self:getinput ("H Angle Down"), self:getinput ("V Base"), self:getinput ("H Base"), self:getinput ("Color")

	if vu and vd and hl and hr and col then
		-- create the 3d objects (gizmos) for horizontal and vertical manipulation
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  true,  false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, col.Value))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  false, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, col.Value))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, true,  false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, col.Value))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, false, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, col.Value))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  true,  true,  vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, col.Value))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, true,  true,  vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, col.Value))
		local	vdu, vdd, hdl, hdr, vdb, hdb = self:getinput ("V Delta Angle Up"), self:getinput ("V Delta Angle Down"), self:getinput ("H Delta Angle Left"), self:getinput ("H Delta Angle Right"), self:getinput ("V Delta Base"), self:getinput ("H Delta Base")

		if vdu and vdd and hdl and hdr and vdb and hdb then
			-- if V Delta Angle and H Delta Angle are present, create the 3d objects
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  true,  false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, col.Value))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  false, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, col.Value))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, true,  false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, col.Value))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, false, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, col.Value))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  true,  true,  vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, col.Value))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, true,  true,  vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, col.Value))
		end
	end
end

function ShaderNodeBarnDoors:getargs ()
	local	vu, vd, hl, hr, vb, hb = self:getinput ("V Angle Up"), self:getinput ("V Angle Down"), self:getinput ("H Angle Left"), self:getinput ("H Angle Right"), self:getinput ("V Base"), self:getinput ("H Base")
	if vu and vd and hl and hr and vb and hb then
		return "frustumprojector", { hl.Value:get (), hr.Value:get (), vu.Value:get (), vd.Value:get (), hb.Value:get (), vb.Value:get () }
	end
end

-- %class ShaderNodeBarnDoorsFallof
-- %desc A frustum gizmo with fallof
class ("ShaderNodeBarnDoorsFallof", "ShaderNodeBarnDoors")
-- Handle all outputs
ShaderNodeBarnDoorsFallof.Variables =
{
	"H Angle Left", "H Angle Right", "V Angle Up", "V Angle Down", "H Delta Angle Left", "H Delta Angle Right", "V Delta Angle Up", "V Delta Angle Down", "H Base", "V Base", "H Delta Base", "V Delta Base"
}

-- Note: Gizmos are only available with interface, it is not legal to subclass them when GAGUI is not true
if GAGUI then

-- Create the GizmoBarnDoors class, from the base class of 3d gizmos LUIGizmo3D
-- LUIGizmo3D objects implement most of the work needed to be done for 3D manipulation
-- in the viewport. Yet, there are a few methods that need to be implemented in order
-- for the gizmo to actually interact with the scene graph. The following methods
-- must be implemented.
class ("GizmoBarnDoors", "Gizmo3D")
-- Callback when the user clicks on the gizmo. This callback lets you initiate the beginning
-- of a click and drag modification. x and y are the mouse coordinates in the window, view
-- is the viewport in which the user clicked.
function GizmoBarnDoors:onLDown (x, y, view)
	-- call the parent class left down handler (required)
	GizmoBarnDoors.Parent.onLDown (self, x, y, view)
	local targetObject = self._TargetObject
	-- the transform of the target primitive
	local mtx = self:getpathworldmatrix (self._Path, targetObject)
	local p0 = mtx:gettranslation ()
	-- the position on the target primitive
	-- distance to target
	local length = targetObject.TargetDistance:get ()
	assert (length)

	self._StartPBase = mtx:transform ({ 0, 0, 0 })
	self._StartP = mtx:transform({0, 0, length})

	-- _StartN is the front vector (Z axis for a target primitive)
	self._StartN = mtx:getk ():getnormalized ()
	-- _StartA is the side axis (X axis if the gizmo operates on horizontal, Y axis otherwise)
	self._StartA = self._IsH and mtx:geti ():getnormalized () or mtx:getj ():getnormalized ()

	self._StartScale = self._IsH and mtx:geti():getlength() or mtx:getj():getlength()
	self._StartD = (p0 - self._StartP):getlength ()
	-- LUI3d2dToPlane projects the x,y coordinates into a 3D position that lies on the 3D plane made by
	-- _StartP and perpendicular to _StartN
	local p = LUI3d2dToPlane (view, x, y, self._StartP, self._StartN)
	local pBase = LUI3d2dToPlane (view, x, y, self._StartPBase, self._StartN)
 	self._StartSignBase = (self._StartA:dotproduct(pBase-self._StartPBase) >= 0 and 1.0 or -1.0)
	self._StartSignAngle = (self._IsFirst and 1.0 or -1.0)
end
-- Callback when the user drags the gizmo. view is the current viewport, x,y are
-- the mouse offset since last call to onDrag, and lx,ly are the mouse coordinates
-- in the viewport
function GizmoBarnDoors:onDrag (view, x, y, lx, ly)

	-- perform the modification
	if self._IsBase then
		-- compute the position of the mouse on the plane
		local p = LUI3d2dToPlane (view, lx, ly, self._StartPBase, self._StartN)

		if self._DeltaBasePlug and self._BasePlug:get() ~= 0 then
		    local vec = p-self._StartPBase

			local newSize = self._StartA:dotproduct ((p-self._StartPBase)) * 2 * self._StartSignBase / self._StartScale
			self:domodification(newSize / self._BasePlug:get())
		else
			local newSize = self._StartA:dotproduct ((p-self._StartPBase)) * 2 * self._StartSignBase / self._StartScale
			self:domodification(newSize)
		end
	else
		-- compute the position of the mouse on the plane
		local p = LUI3d2dToPlane (view, lx, ly, self._StartP, self._StartN)
		-- compute the angle of this point to the center axis

		if self._DeltaAnglePlug then
			-- if the gizmo is a delta gizmo, then compute the delta from the new angle
			-- and the outer angle
			local opp = (self._StartA:dotproduct (p - self._StartP) - self._StartSignAngle * self._DeltaBasePlug:get() * self._BasePlug:get() * 0.5 * self._StartScale)
			local newAngle = math.atan (opp / self._StartD) * self._StartSignAngle
			self:domodification (self._AnglePlug:get () - newAngle)
		else
			local tan = (self._StartA:dotproduct (p - self._StartP) - self._StartSignBase * self._BasePlug:get() * 0.5 * self._StartScale) / self._StartD
			local newAngle = math.atan (tan) * self._StartSignBase
			-- if the gizmo is the outer gizmo, then just modify with the computed angle
			self:domodification (newAngle)
		end
	end

	return 0,0
end
-- Callback when the viewport wants to read the current gizmo value
function GizmoBarnDoors:onGetGizmoValue ()

	if self._IsBase then
		if self._DeltaBasePlug then
			return self._DeltaBasePlug:get()
		else
			return self._BasePlug:get()
		end
	end

	if self._DeltaAnglePlug then
		-- for delta gizmo, return the delta plug value
		return self._DeltaAnglePlug:get ()
	else
		-- for outer gizmo, return the angle
		return self._AnglePlug:get ()
	end
end
-- Callback when the viewport wants to change the current gizmo value
-- This function is always called in a modification context, that is
-- you have to get a modifier on the document and use this to change the
-- associated plug value
function GizmoBarnDoors:onSetGizmoValue (value)
	local	mod = Document:getmodifier ()
	if self._IsBase then
		if self._DeltaBasePlug then
			mod.set(self._DeltaBasePlug, math.min(1.0, math.max(value, 0.0)))
		else
			mod.set(self._BasePlug, math.max(value, 0))
		end
	elseif self._DeltaAnglePlug then
		mod.set (self._DeltaAnglePlug, math.max (value, 0))
	else
		mod.set (self._AnglePlug, math.min (math.max (value, 0), 90))
	end
end
-- Callback when the system wants the object to evaluate a plug value
function GizmoBarnDoors:eval (plug)
	if plug == self.Transform then
		-- The gizmo transform is the associated object transform
		local targetObject = self._TargetObject
		--return transform.create (targetObject.Transform:get ())
		return transform.create (true)
	elseif plug == self.MeshFlags then
		-- no specific mesh flags
		return 0
	elseif plug == self.MeshScaleFactor then
		-- don't change the gizmo mesh scale
		return 1
	elseif plug == self.Mesh then
		-- build the gizmo mesh
		local targetObject = self._TargetObject
		local vuangle = self._VUAnglePlug:get ()
		local vdangle = self._VDAnglePlug:get ()
		local hlangle = self._HLAnglePlug:get ()
		local hrangle = self._HRAnglePlug:get ()
		local vbase = self._VBasePlug:get()
		local hbase = self._HBasePlug:get()
		if self._DeltaAnglePlug then
			vuangle = vuangle - self._VDUAnglePlug:get ()
			vdangle = vdangle - self._VDDAnglePlug:get ()
			hlangle = hlangle - self._HDLAnglePlug:get ()
			hrangle = hrangle - self._HDRAnglePlug:get ()
		end
		if self._DeltaBasePlug then
			vbase = vbase * self._VDBasePlug:get()
			hbase = hbase * self._HDBasePlug:get()
		end
		local length = targetObject.TargetDistance:get ()
		local dir = targetObject.DirectionMode:get () == "-z" and -1 or 1
		-- this function builds the 3D object for this gizmo
		return GizmoBarnDoorsBuildMesh (self, length*dir, self._IsH, self._IsFirst, self._IsBase, vuangle, vdangle, hlangle, hrangle, vbase, hbase)
	end
	-- the plug could not be evaluated, let the parent class evaluate the plug
	return self:parenteval (plug, "LUIGizmo3D")
end
-- Retrieve the GL properties of the gizmo
function GizmoBarnDoors:getGLprops ()
	return GLdisplay.OverrideDisplayMode+GLdisplay.NodeLocal+self._TargetObject:getGLnodetype (), 1
end

-- We create a subclass for each kind of gizmos. This is important because
-- the manipulation system automatically performs the modification on all
-- active gizmos of the same class. So, if all gizmos were of the same class
-- dragging a gizmo would cause all gizmos of this class to react, and it wouldn't
-- be possible to separately change the frustum height and width.
class ("GizmoHLBarnDoors", "GizmoBarnDoors")
class ("GizmoHRBarnDoors", "GizmoBarnDoors")
class ("GizmoVUBarnDoors", "GizmoBarnDoors")
class ("GizmoVDBarnDoors", "GizmoBarnDoors")
class ("GizmoHDLBarnDoors", "GizmoBarnDoors")
class ("GizmoHDRBarnDoors", "GizmoBarnDoors")
class ("GizmoVDUBarnDoors", "GizmoBarnDoors")
class ("GizmoVDDBarnDoors", "GizmoBarnDoors")
class ("GizmoHBBarnDoors", "GizmoBarnDoors")
class ("GizmoVBBarnDoors", "GizmoBarnDoors")
class ("GizmoHDBBarnDoors", "GizmoBarnDoors")
class ("GizmoVDBBarnDoors", "GizmoBarnDoors")

-- This function create a simple gizmo. A whole frustum is composed of 2 gizmos, one for
-- horizontal manipulation and one for vertical. Fallof gizmo is an additionnal 2 gizmos.
function GizmoBarnDoorsCreate (tool, targetObject, isH, isFirst, isBase, vuPlug, vdPlug, hlPlug, hrPlug, vbPlug, hbPlug, vduPlug, vddPlug, hdlPlug, hdrPlug, vdbPlug, hdbPlug, colorPlug)
	-- Create a 3D gizmo that holds 1 mesh

	local	self = LUIGizmo3DCreate (tool,
		{
			LUIGizmoMeshCreate (nil, { 1, 0, 1, 1 }),
		}, targetObject)
	-- Set the accurate class depending on the kind of gizmo that is required

	if isBase then
		if vdbPlug and hdbPlug then
			setclass (self, isH and GizmoHDBBarnDoors or GizmoVDBBarnDoors)
		else
			setclass (self, isH and GizmoHBBarnDoors or GizmoVBBarnDoors)
		end
	elseif isH then
		if isFirst then
			if hdrPlug then
				setclass(self, GizmoHDRBarnDoors)
			else
				setclass(self, GizmoHRBarnDoors)
			end
		else
			if hdlPlug then
				setclass(self, GizmoHDLBarnDoors)
			else
				setclass(self, GizmoHLBarnDoors)
			end
		end
	else
		if isFirst then
			if vduPlug then
				setclass(self, GizmoVDUBarnDoors)
			else
				setclass(self, GizmoVUBarnDoors)
			end
		else
			if vddPlug then
				setclass(self, GizmoVDDBarnDoors)
			else
				setclass(self, GizmoVDBarnDoors)
			end
		end
	end

	assert (targetObject)
	assert (colorPlug)

	self._IsH = isH
	self._IsBase = isBase
	self._IsFirst = isFirst
	self._TargetObject = targetObject
	self._VUAnglePlug = vuPlug
	self._VDAnglePlug = vdPlug
	self._HLAnglePlug = hlPlug
	self._HRAnglePlug = hrPlug
	self._VBasePlug = vbPlug
	self._HBasePlug = hbPlug
	self._VDUAnglePlug = vduPlug
	self._VDDAnglePlug = vddPlug
	self._HDLAnglePlug = hdlPlug
	self._HDRAnglePlug = hdrPlug
	self._VDBasePlug = vdbPlug
	self._HDBasePlug = hdbPlug
	self._BasePlug = isH and hbPlug or vbPlug
	self._DeltaBasePlug = isH and hdbPlug or vdbPlug
	self._AnglePlug = isH and (isFirst and hrPlug or hlPlug) or (isFirst and vuPlug or vdPlug)
	self._DeltaAnglePlug = isH and (isFirst and hdrPlug or hdlPlug) or (isFirst and vduPlug or vddPlug)

	-- The mesh plug
	Plug (self, "Mesh", Plug.NoSerial, types.mesh)

	-- the gizmo mesh is dependent on the input plugs
	self.Mesh:adddependencies (vbPlug, hbPlug, vuPlug, vdPlug, hlPlug, hrPlug, targetObject.TargetDistance, targetObject.DirectionMode)

	if vduPlug and vddPlug and hdlPlug and hdrPlug and hdbPlug and vdbPlug then
		self.Mesh:adddependencies (vduPlug, vddPlug, hdlPlug, hdrPlug, hdbPlug, vdbPlug)
	end
	-- the gizmo transform is dependent on the associated object transform
	self.Transform:set (transform.create (true))
	-- connect the gizmo to the given input plugs
	self._Meshes[1].Geometry.Mesh:connect (self.Mesh)
	self._Meshes[1].GeometryTransparent.Mesh:connect (self.Mesh)
	self._Meshes[1].Color:connect (colorPlug)
	return self
end

setglobal ("GizmoBarnDoorsCreate", GizmoBarnDoorsCreate)

-- Build the actual 3D mesh for the gizmo
function GizmoBarnDoorsBuildMesh (self, length, isH, isFirst, isBase, vuangle, vdangle, hlangle, hrangle, vbase, hbase)

	local	hu, hd = length * math.tan(vuangle), length * math.tan(vdangle)
	local	wl, wr = length * math.tan(hlangle), length * math.tan(hrangle)

	local ho = vbase * 0.5
	local wo = hbase * 0.5

	local line = LUIMeshCreate ()

	if isBase then
		if isH then
			LUIMeshAddPolyLine (line, { -wo, ho, 0, -wo, -ho, 0}, false)
			LUIMeshAddPolyLine (line, { wo, ho, 0, wo, -ho, 0}, false)
		else
			LUIMeshAddPolyLine (line, { -wo, ho, 0, wo, ho, 0}, false)
			LUIMeshAddPolyLine (line, { -wo, -ho, 0, wo, -ho, 0}, false)
		end
	else
		if isH then
			-- horizontal gizmo mesh
			if isFirst then
			    LUIMeshAddPolyLine (line, { wo, ho, 0,  wr+wo,  hu+ho, length}, false)  -- Top right
				LUIMeshAddPolyLine (line, { wo, -ho, 0, wr+wo, -hd-ho, length}, false)  -- Bottom right
				LUIMeshAddPolyLine (line, { wr+wo,  hu+ho, length, wr+wo, -hd-ho, length}, false)  -- Link
			else
				LUIMeshAddPolyLine (line, { -wo, ho, 0,  -wl-wo,  hu+ho, length}, false)  -- Top left
				LUIMeshAddPolyLine (line, { -wo, -ho, 0, -wl-wo, -hd-ho, length}, false)  -- Bottom left
				LUIMeshAddPolyLine (line, { -wl-wo, hu+ho, length, -wl-wo, -hd-ho, length}, false)  -- Link
			end
		else
			-- vertical gizmo mesh
			if isFirst then
				LUIMeshAddPolyLine (line, {-wl-wo,  ho+hu, length, wo+wr, ho+hu, length}, false)
			else
				LUIMeshAddPolyLine (line, {-wl-wo,  -ho-hd, length, wo+wr, -ho-hd, length}, false)
			end
		end
	end

	local mesh, laabb = glmesh.createlines (line, 0)
	return { mesh, laabb }
end

end -- GAGUI
