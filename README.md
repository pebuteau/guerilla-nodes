# Introduction


This guerilla-nodes repository contains a collection of shaders and other
utility nodes for the rendering engine [Guerilla Render](http://guerillarender.com/).  
This is not an official repository.

### How to use it
1) Clone or download the project,
2) Add the path of the library folder to your guerilla configuration file.  
```
UserLibrary = /home/mac/Documents/vscode/guerilla-nodes/library
```  
More information from the guerilla documentation [here](http://guerillarender.com/doc/2.2/TD%20Guide_Technical%20Notes_User%20Library.html?highlight=userlibrary).

[Quick tutorial about library](https://youtu.be/nNPc4WeyOfo)

***Important Notes:***

Some plugins of the guerilla-nodes repository need to access the content of
the library. By default, the plugins expect to find the library folder in
the same folder as the plugins folder.

If the library is at another
location, set the GN_PLUGINS_LIBRARY to this location (for example:
    /home/mac/Documents/my_guerilla_libraries/guerilla-nodes/library)

**Enjoy!**

<img src="https://media.giphy.com/media/Zw3oBUuOlDJ3W/giphy.gif" width="180" />

#### Contributions and love from
Cyril Corvazier  
Benjamin Legros  
Paul-Emile Buteau  
Philippe Llerena  
You ?


# Documentation

[Attributes](#attributes)  
[Nodes](#nodes)  
[Materials](#materials)
[Lights](#lights)

Attributes
---------------------------

The attribute nodes are usually SL shaders that you can use as input to most
material attributes (such as Diffuse Color, Roughness, Bump, etc...).  
You can use them directly by clicking the <img src="doc/images/MButtonShaderAttribute.png" align="center"> button next to every material attribute and then select the shader you want to use.  
The attribute nodes are also accessible from the Node Picker in any sub-shader graph by using the shortcut Ctrl+Space.
You can find more information on how to use custom shaders on the Guerilla documentation [here](http://guerillarender.com/doc/2.1/User%20Guide_Shading_Sub-Shaders%20and%20Textures.html).

The source code of all the shaders is accessible and editable by double clicking the SLShader node in subshader graphs.

#### AlligatorNoise2D
Generates an Alligator type noise.
The implementation is inspired by the [Houdini version](https://www.sidefx.com/docs/hdk/alligator_2alligator_8_c-example.html).

| No octave | 2 octaves | No octave animation | 2 octave animation |
|:--:|:--:|:--:|:--:|
| <img src="doc/images/AlligatorNoise2DNoOctave.png" width="150"/> | <img src="doc/images/AlligatorNoise2D2Octave.png" width="150"/> | <img src="doc/images/AlligatorNoise2DNoOctaveAnim.gif" width="150"/> |<img src="doc/images/AlligatorNoise2D2OctaveAnim.gif" width="150"/> |
| <img src="doc/images/AlligatorNoise3DNoOctave.png" width="150"/> | <img src="doc/images/AlligatorNoise3D2Octave.png" width="150"/> | <img src="doc/images/AlligatorNoise3DNoOctaveAnim.gif" width="150"/> |<img src="doc/images/AlligatorNoise3D2OctaveAnim.gif" width="150"/> |

#### EulerTransform
Utility node that performs an Euler Transform on the input coordinates.
This is useful if you need to do the shading in a custom local coordinate system.

#### HashVector
Utility node that returns a random float, vector or color based on the given input vector.

#### InsideCameraFrustum
This node returns 1 when the shading point is within the given camera frustum.

#### TextureScatter
Scatters the given textures randomly on the given input coordinates space.
It can be used to generate complex and high definition procedural textures from many low resolution textures.

The shader provides the following features:
* Up to 100 textures input to be scattered
* Mask texture with threshold control
* Controllable density, size and dispersion of elements
* Randomized Rotation and Scale
* Color blending normalization

| Input textures (32x32)| Mask (32x32) | Result (512x512)|
|:--:|:--:|:--:|
| <img src="doc/images/sprite.1.png"/> <img src="doc/images/sprite.2.png"/> <img src="doc/images/sprite.3.png"/> <img src="doc/images/sprite.4.png"/> | No mask | <img src="doc/images/TextureScatterNoMask.png" width="200"/> |
| <img src="doc/images/sprite.1.png"/> <img src="doc/images/sprite.2.png"/> <img src="doc/images/sprite.3.png"/> <img src="doc/images/sprite.4.png"/> | <img src="doc/images/sprite.2.png"/> | <img src="doc/images/TextureScatterMask.png" width="200"/> |

***Important Notes:***

This shader requires a continuous input space to give artifact free result.
Therefore, this shader does not support UDIM very well especially if there are cuts in the middle of a geometry. 

This is not a cheap shader as it requires up to 9 texture lookups per sample.
**To get better performance, it is recommended to bake the resulting textures
using the Bake2D passes of Guerilla.**

Because the TextureScatter node is based on a Voronoï grid, increasing the Size parameter above 1 will drastically increase the render time.
The computation complexity is proportional to ceil(Size)^2, which means that: 
For a size between 0 and 1, the number of texture lookups required is up to 9.
For a size between 1 and 2, the number of texture lookups required is up to 16.
To get bigger elements, it is wiser to play with the Scale and Density attributes first.
To counter the complexity due to the increase of Size, you may also adjust the advanced parameter Grid Kernel Size. However, this can introduce artifacts.

#### TextureTile
Tile the given texture with some randomness.

The shader provides the following features:
* Randomized Dispersion, Rotation and Scale
* Edge blending with contrast adjustment

| Classic tiling | Randomized tiling |
|:--:|:--:|
| <img src="doc/images/TextureTileNoRandomness.png" width="200"/> | <img src="doc/images/TextureTileWithRandomness.png" width="200"/> |

***Important Notes:***

This shader requires a continuous input space to give artifact free result.
Therefore, this shader does not support UDIM very well especially if there are cuts in the middle of a geometry. 

This is not a cheap shader as it requires up to 9 texture lookups per sample.
**To get better performance, it is recommended to bake the resulting textures
using the Bake2D passes of Guerilla.**

#### TextureSwitchLoop
Same as the native Texture Switch Guerilla node but which loops over the textures.
For example, if 2 textures are provided but the attribute used to switch texture goes from 1 to 5,
the textures will be distributed like this:  
id 1 -> texture 1  
id 2 -> texture 2  
id 3 -> texture 1 (goes back to 1 here)  
id 4 -> texture 2  
id 5 -> texture 1 (goes back again)  
so on and so forth...

#### TextureSwitchNormal
Same as the native [TextureSwitch](http://guerillarender.com/doc/2.2/Library_Attributes_TextureSwitch.html?highlight=textureswitch) Guerilla node but for normal maps.

#### VectorShifter
Utility node which allows to swap components of a given vector.
It does the same as the combination of the nodes 'From RGBA' and 'To RGBA' but with a single node for convenience.

<a name="voronoi2d"></a>
#### Voronoi2D
This node generates 2D Voronoï cells and can returns the following outputs:

| Cell Coordinates | Cell Position | Cell Index | Border Distance |
|:--:|:--:|:--:|:--:|
| <img src="doc/images/Voronoi2DCellCoords.png" width="150"/> | <img src="doc/images/Voronoi2DCellPosition.png" width="150"/> | <img src="doc/images/Voronoi2DCellIndex.png" width="150"/> |<img src="doc/images/Voronoi2DCellDistanceToBorder.png" width="150"/> |

The shader provides the following features:
* Randomisation of cell rotation and scale
* Border Distance smoothness
* Separate seeds for each randomisation

#### Voronoi2DRecursive
This node generates 2D recursive Voronoï cells and returns only the cell coordinates for each cell

| Cell Coordinates <br> 0 octave | Cell Coordinates <br> 1 octave |
|:--:|:--:|
| <img src="doc/images/Voronoi2DRecursiveCellCoordsOct0.png" width="150"/> | <img src="doc/images/Voronoi2DRecursiveCellCoordsOct1.png" width="150"/> |

The shader provides the following features:
* Randomisation of cell rotation
* Octave scaling of scale, jitter and random rotation
* Separate seeds for each randomisation

#### Voronoi3D
Same as the [Voronoi2D](#voronoi2d) node but in three dimensions.

Nodes
=====

#### Bump
Simple bump node that can take any input bump value.
Here's an example on how to easily create bump details with a simple cloud node in a sub-shader graph:

<img src="doc/images/BumpNodeUsage.png" width="850"/>

#### PbrBlendMat
Node that correctly blends the diffuse Color, roughness, metal ans normals for two sets of values.

Materials
=========

Materials are complete shading solution.  
Use the Node Picker (shortcut: Ctrl+Space) in a Render Graph to assign a material to a node.


#### Eye2Plus
Improved version of the native Eye2 shader of Guerilla.

The Eye2Plus material fixes a bug in the Id AOV of the native Eye2 material:

| Native Eye2 Id AOV | Eye2Plus Id AOV |
|:--:|:--:|
| <img src="doc/images/Eye2BugIdCone.png" width="150"/> | <img src="doc/images/Eye2PlusIdCone.png" width="150"/> |

The Eye2Plus material adds the following features to the native Eye2 material:

| Default | Iris Incandescence | Pupil Diffuse | Pupil Incandescence | Pupil Specular |
|:--:|:--:|:--:|:--:|:--:|
| <img src="doc/images/Eye2PlusClassic.png" width="150"/> | <img src="doc/images/Eye2PlusIrisIncandescence.png" width="150"/> | <img src="doc/images/Eye2PlusPupilDiffuse.png" width="150"/> | <img src="doc/images/Eye2PlusPupilIncandescence.png" width="150"/> | <img src="doc/images/Eye2PlusPupilSpec.png" width="150"/> |

***Important Notes:***

* Because the Eye2Plus material (as the Eye2 material) uses the internal SurfaceCore shader of Guerilla,
you must use a specific version of the Eye2Plus material depending on your Guerilla version:  
Use the Eye2PlusOld material if you use a version prior to 2.2.  
Use the Eye2Plus material if you use the version 2.2.
This material may not work on newer version of Guerilla. We will try to
keep it up to date. ;-)

* Pupil shading effects are not yet well supported in presence of pupil blur.

Lights
======

***Important Notes:***

The evaluation of those custom lights are not as optimized as the native
guerilla lights. You should expect an average increase of light sampling time of
10% to 15% compared to classic light. Do not use those custom lights where
you could use a native one.

#### Barn Doors Square Light
Square light with a custom light shader that acts like barn doors.
The four sides of the gizmo uses independent size and smoothness controllers.

<img src="doc/images/BarnDoorsLightDemo.gif"/>
